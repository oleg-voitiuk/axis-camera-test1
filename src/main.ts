import {Connection, Protocol, Parameters, UserAccounts} from 'axis-configuration';

const address = process.env.ADDRESS || '192.168.1.102';
const port = +process.env.PORT || 80;
const user = process.env.USER || 'root';
const password = process.env.PASSWORD || 'test';

const connection = new Connection(Protocol.Http, address, port, user, password);

new Parameters(connection)
    .get('*')
    .then(result => console.log('Params: ', result))
    .catch(err => console.log(err));
new UserAccounts(connection)
    .getAll()
    .then(users => console.log('Users: ', users))
    .catch(err => console.log(err));
